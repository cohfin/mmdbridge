#include <Windows.h>
#include <ImageHlp.h>

template <typename FuncType>
bool hook_dllimp(LPCSTR module_name, LPCSTR import_name, FuncType *hook_func, FuncType **original_func)
{
	HMODULE mod = ::GetModuleHandle(NULL);
	FARPROC org_func = ::GetProcAddress(::GetModuleHandleA(module_name), import_name);

	ULONG imp_size;
	IMAGE_IMPORT_DESCRIPTOR *import_desc;
	import_desc = reinterpret_cast<IMAGE_IMPORT_DESCRIPTOR *>(::ImageDirectoryEntryToData(mod, TRUE, IMAGE_DIRECTORY_ENTRY_IMPORT, &imp_size));

	for (; import_desc->Name; ++import_desc)
	{
		char *name = reinterpret_cast<char *>(mod)+import_desc->Name;
		if (!::lstrcmpiA(name, module_name))
			break;
	}
	if (!import_desc->Name)
		return false;

	IMAGE_THUNK_DATA *iat = reinterpret_cast<IMAGE_THUNK_DATA *>(reinterpret_cast<char *>(mod)+import_desc->FirstThunk);

	for (; iat->u1.Function; ++iat)
	{
		FARPROC *func = reinterpret_cast<FARPROC *>(&iat->u1.Function);
		if (*func == org_func)
		{
			DWORD protect;
			::VirtualProtect(func, sizeof(func), PAGE_READWRITE, &protect);
			*func = reinterpret_cast<FARPROC>(hook_func);
			::VirtualProtect(func, sizeof(func), protect, &protect);

			*original_func = reinterpret_cast<FuncType *>(org_func);
			return true;
		}
	}

	return false;
}

